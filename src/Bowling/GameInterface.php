<?php
namespace Bowling;

interface GameInterface
{
    /**
     * @param int $pins - The amount of pins that were knocked down on a single roll
     */
	public function roll($pins);

    /**
     * @returns int - The score for the current game
     */
	public function score();
}
