Feature: Calculate the score of a bowling game
    A Score can only be calculated once the Game is finished
    A Game is finished after 10 Frames
    A Frame has 10 Pins that should be knocked down
    A Player make up to 2 attempts (Turn) per Frame to knock down Pins
    A Frame Score is the number of the Pins knocked down plus a bonus for Strikes and Spares
    A Strike occurs when a Player knocks down all Pins in a single Roll
    A Spare occurs when a Player knocks down all Pins in two Rolls
	A Player can make an extra attempt (Turn) if he scores a Strike or a Spare on Frame 10

    Scenario: Roll a gutter game
        Given A Player knocks no pins down
		When The Game is Over
        Then The Score should be zero

    Scenario: Roll a Strike
        Given A bowler knocked down all pins
        And This was the 1st Roll for a Frame
        Then The Bonus for the current Frame is the Score of the next two Rolls

    Scenario: Roll a Spare
        Given A bowler knocked down all pins
        And This was the 2nd Roll for a Frame
        Then The Bonus for the current Frame is the Score of the next Roll

    Scenario: Roll a perfect game
        Given A bowler rolled only strikes
        When The Game is Over
        Then The total Score should be 300
