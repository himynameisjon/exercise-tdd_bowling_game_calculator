<?php namespace Bowling;

class GameTest extends \PHPUnit_Framework_TestCase
{
    public function testGameExists() {
        $this->assertInstanceOf(Game::class, new Game());
    }
}
